﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Interspecific")]
[assembly: AssemblyProduct("Interspecific")]
[assembly: AssemblyDescription("Interspecific REST HTTP server framework. See https://gitlab.com/derbyinsight/Interspecific")]
[assembly: AssemblyCompany("Interspecific project contributors")]
[assembly: AssemblyCopyright("© 2014-2021 Scott Offen and other project contributors")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.0.4.*")]
[assembly: AssemblyFileVersion("2.0.4.0")]
